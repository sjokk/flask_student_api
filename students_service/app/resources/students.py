from app import api
from app.classes.students import Students
from flask_restx import Resource, fields

def studentOrAbort(student):
    if student is None:
        api.abort(404, f'Student with id {id} does not exist')

    return student

# The namespace has meaning for the swagger/openapi documentation
# The whole namespace is now prefixed with /students
ns = api.namespace('students', description='STUDENT operations')

# Description of how the model is rendered
# Skipping attributes privatizes them this way
student = api.model('Student', {
    'id': fields.Integer(
        readonly=True,
        example=100, 
        min=1,
        description='The student\'s unique identifier'), # these are directly mapped to the class (the keys -> attributes)
    # In case a property is spelled incorrectly or doesnt exist, it returns null
    'number': fields.Integer(
        example=3247728,
        min=1,
        required=True,
        description='The student number, can be found on student card'
    ),
    'full_name': fields.String(
        attribute='name', 
        required=True, 
        min_length=1, 
        max_length=64, 
        example='Barnaby Jones',
        description='The student\'s full name as <first_name> <last_name>'), # Exposed attribute key = full_name and the accessed attribute = name (attribute of the obj)
    # Add the url for each specific individual resource
    # Not sure why but makes all CUD operations crash, since it can't create
    # a url_for those models.
    # 'url': fields.Url(
    #     'student_resource',
    #     readonly=True, 
    #     absolute=True,
    #     description='Absolute URL to the specific student resource')
})

studentsDAO = Students()

# path to the specified resource
@ns.route('/')
@ns.doc(description='A collection of student resources')
# Inherit from Resource class
class Students(Resource):
    '''Shows the collection of all students and allows you 
    to create and add new students'''
    # marshal_with (model) is used to describe how the model
    # is serialized, adding it to the @api adds swagger documentation.
    @ns.marshal_list_with(student, envelope='students')
    # These methods should translate to RESTful HTTP methods.
    def get(self):
        # The docstring is used to document the API as well
        '''returns the collection of all students.'''
        return studentsDAO.getAll()

    # indicating we expect and accept a json object with an id field and a name
    # We want all fields posted to be validated
    @ns.doc(model=student)
    @ns.expect(student, validate=True)
    @ns.response(201, 'Student successfully created', student)
    @ns.marshal_with(student, code=201)
    def post(self):
        '''Create a new student'''
        return studentsDAO.add(api.payload), 201


# Provide a name for the endpoint, that can be used to include uris or urls.
@ns.route('/<int:id>', endpoint='student_resource')
# ns.response(404) provides the description of what happened when a 404 response is returned
# For documentation purposes
@ns.param('id', 'The student identifier')
@ns.doc(description='A student resource')
class Student(Resource):
    @ns.doc('get_student')
    @ns.response(200, 'Student successfully found', student)
    @ns.response(404, 'Student not found')
    @ns.marshal_with(student, 200)
    def get(self, id):
        '''returns an individual student with given id'''
        student = studentsDAO.get(id)
        
        return studentOrAbort(student)
    
    @ns.doc('update_student')
    @ns.response(200, 'Student successfully updated', student)
    @ns.response(404, 'Student not found')
    @ns.expect(student, validate=True)
    @ns.marshal_with(student)
    def put(self, id):
        '''Update a student by identifier'''
        student = studentsDAO.update(id, api.payload)

        return studentOrAbort(student)

    @ns.doc('delete_student')
    @ns.response(200, 'Student successfully deleted', student)
    @ns.response(404, 'Student not found')
    @ns.marshal_with(student)
    def delete(self, id):
        '''Delete a student by identifier'''
        student = studentsDAO.delete(id)
        
        return studentOrAbort(student)
