from __future__ import annotations
from app.models.student import Student

class Students():
    def __init__(self):
        self.students = [
            Student(3235661, 'Vlad Luca'),
            Student(3313859, 'Kereem Coipel'),
            Student(3255557, 'Miguel Boekhold'),
            Student(2158030, 'Remy Bos'),
            Student(3463555, 'Bas Joosten'),
            Student(2883066, 'Jorge Barrasa'),
        ]

    def add(self, student) -> None:
        student = Student(student['number'], student['full_name'])
        self.students.append(student)
        return student

    def get(self, id: int) -> Student:
        for student in self.students:
            if student.id == id:
                return student
    
    def getAll(self) -> list:
        return self.students

    def update(self, id, student):
        target = self.get(id)
        
        if target is not None:
            target.number = student['number']
            target.name = student['full_name']

        return target

    def delete(self, id):
        for student in self.students:
            if student.id == id:
                self.students.remove(student)
                return student
