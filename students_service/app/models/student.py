class Student():
    id = 0

    def __init__(self, number: int, name: str):
        self.id = Student.getId()
        self.number = number
        self.name = name

    @classmethod
    def getId(cls) -> int:
        cls.id += 1
        return cls.id

    def __str__(self) -> str:
        return f'({self.number}) {self.name}'