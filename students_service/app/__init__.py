from flask import Flask, Blueprint
from flask_restx import Api

app = Flask(__name__)

api_bp = Blueprint('api', __name__, url_prefix='/api')

api = Api(
    api_bp, 
    version='1.0', 
    title='STUDENTS API',
    description='A simple STUDENT API',
    doc='/documentation'
)

app.register_blueprint(api_bp)

from app.resources import students
