FROM python:3.7-alpine
WORKDIR /students_service
ENV FLASK_APP students_api.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY ./students_service .
CMD ["flask", "run"]